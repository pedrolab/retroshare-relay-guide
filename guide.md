# Setting up a Retroshare relay to propagate contents

This step-by-step guide search to explain how to setup a Retroshare node that will act as relay 24/7 to propagate content and connect users on networks that doesn't have a big "critical mass". As decentralized network, Retroshare doesn't need a server that gives to the user the information, because are the users who store and share it. Unfortunately if the F2F network is little, the contents could arrive slowly to all the network parts.

To solve this you can configure a Retroshare node that will act as relay to help the F2F network until is grown. Want to accomplish two objectives:

1. To accept automatically certificates of users that wants to enter the network
2. Propagate contents between the users of this network (such as forums)

#### Index
1. [Configuring RetroShare](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#configuring-retroshare)
    1. [Installation](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#installation)
    2. [Running first time](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#running-first-time)
    3. [Subscribing contents](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#subscribing-contents)
2. [Setting up automatic key acceptation](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#setting-up-automatic-key-acceptation)
3. [Automatizing](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#automatizing)
    1. [Running Retroshare as service](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#running-retroshare-as-service)
    2. [Configuring `login.sh`](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#configuring-loginsh)
    3. [Setting up `buryTheDead.py`](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#setting-up-burythedeadpy)
4. [Best configurations](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#best-configurations)
    1. [Forwarding ports to direct connection](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#forwarding-ports-to-direct-connection)
5. [Some other notes (TL;DR)](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#some-other-notes-tldr)
    * [Moving and backuping locations](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#moving-and-backuping-locations)
    * [Configuring cron](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#configuring-cron)
    * [RetroShare API](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#retroshare-api)
    * [Dynamic Ip and port forwarding.](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/guide.md#dynamic-ip-and-port-forwarding)


## Configuring Retroshare

### Installation

First of all add [Retroshare repos](http://retroshare.net/downloads.html) depending of your distro. Nota that for Ubuntu packages add the **OBS repository**.

* For Debian:
    ```
    export DEBIAN_MAJOR_VERSION=$(source /etc/os-release ; echo $VERSION_ID | awk -F. '{print $1}')
    wget -qO - http://download.opensuse.org/repositories/network:retroshare/Debian_${DEBIAN_MAJOR_VERSION}.0/Release.key | sudo apt-key add -
    sudo sh -c "echo 'deb http://download.opensuse.org/repositories/network:/retroshare/Debian_${DEBIAN_MAJOR_VERSION}.0/ /' >> /etc/apt/sources.list.d/retroshare.list"
    ```

* On Debian9 repos
    ```
    sudo apt install retroshare-service
    ```

* On Ubuntu, check that you have the OBS repositories added correctly. http://retroshare.net/downloads.html

    Additionally for **linux mint**, what version of Ubuntu is based on using:
    ```
    cat /etc/upstream-release/lsb-release
    ```

    Then hardcode it on commands to add the repository on link above.

* If you get the __ERROR__: `trying to overwrite  '/usr/share/retroshare/bdboot.txt', which is also in package retroshare 0.6.4` a possible solution could be to download the .deb package and force the installation with:
    ```
    wget https://download.opensuse.org/repositories/network:/retroshare/xUbuntu_18.04/amd64/retroshare-service_0.6.5_amd64.deb #Select the package of your distro
    sudo dpkg -i --force-overwrite retroshare-service_0.6.5_amd64.deb
    ```

### Running first time

1. Create Retroshare user

    ```
    sudo adduser retroshare
    su retroshare
    ```

2. Start Retroshare service as Retroshare user (**TODO**: give permisions and get Retroshare user working properly)

    ```
    retroshare-service
    ```

3. Check if Retroshare profile exists and you are logged in:

    ```
    curl http://127.0.0.1:9092/rsAccounts/getCurrentAccountId
    curl http://127.0.0.1:9092/rsLoginHelper/isLoggedIn
    ```

4. Create or login to your profile:

    * If you want to **create** Retroshare profile. After create the identity  will log in automatically.

        ```
        curl -d '{ "location":{"mLocationName":"prueba","mPpgName":"prueba"},"password":"prueba" }' http://127.0.0.1:9092/rsLoginHelper/createLocation
        ```

    * If you want to **log in** first get the account id (mLocationId):

        ```
        curl http://127.0.0.1:9092/rsLoginHelper/getLocations
        ```

        And then use it to login:

        ```
        curl -d '{ "account":"f943c89102a9ac246a246fe4f6a6e844","password":"prueba" }' http://127.0.0.1:9092/rsLoginHelper/attemptLogin
      ```

5. Interchange the keys for **first time**. (OPTIONAL: this is optional because you can interchange the keys directly setting up web interface)

    1. First get your own certificate. You will need to parse it with "jq" (`sudo apt install jq`) because the backend returns the certificate with '\n' character instead of break line.

        ```
        curl -u "f943c89102a9ac246a246fe4f6a6e844:prueba"  http://127.0.0.1:9092/rspeers/GetRetroshareInvite | jq -j '.retval'
        ```

        Once you get the certificate add it to other node.

    2. Import a certificate to the node. First of all save your Retroshare key into a file (myinvite.txt)

        ```
        cat myinvite.txt | sed ':a;N;$!ba;s/\n/\\n/g' > myinvite.txt
        curl -u "f943c89102a9ac246a246fe4f6a6e844:prueba" -d '{ "invite" : "'$(cat myinvite.txt)'" }'  http://127.0.0.1:9092/rspeers/acceptInvite
        ```

Now you will have two friend nodes propagating contents to each other.

### Subscribing contents

After a while of have a friend node your Retroshare service will start to receive content to subscribe. For this node, just subscribing to a content will start to propagate it to his friend nodes, and the users will see its without having the invite of anybody.

#### Forums

**TODO** : @jsonapi for forums is developed on https://github.com/RetroShare/RetroShare/pull/1401 and available on `retroshare-service`.

The @jsonapi is not developed already in this area, so the best solution is to use the GUI to subscribe the node to a forum.

1. Close the service and start the gui. You should see your new profile on the profile menu.
2. Subscribe to the forum as normal.

#### Channels

Look at script from Tier1 repository [channels_update.qml](https://gitlab.com/RepositorioDeCultura/Tier1/blob/master/channels_update.qml).

Look at [`channelsCheatset.md`](https://gitlab.com/pedrolab/retroshare-relay-guide/blob/master/channelsCheatset.md) to learn how to with curl commands.

#### File sharings 
* Get shared directories
```bash
curl -u "$ACCOUNT:$PASSWORD" http://127.0.0.1:9092/rsFiles/getSharedDirectories
```

* Share directory
```bash
curl -u "$ACCOUNT:$PASSWORD" -d '{ "dir" : { "filename" : "/path/to/dir" , "virtualname" : "virtname", "shareflags" : 128 }, "caller_data":""}' http://127.0.0.1:9092/rsFiles/addSharedDirectory
Or a list of directories with endpoint setSharedDirectories
```


## Setting up automatic key acceptation

We can use the repository of [retroshare-web-bridges](https://gitlab.com/r3sistance/retroshare-web-bridges). There are instrucions of how to install the frontend.

See a working example here: https://retroshare.guifi.net:8080

This web interface permit to the user to automatically add his Retroshare invite to the node via a POST request to Retroshare API.

### Setting up web interface

##### Enable Nginx communication with the API

To let Nginx to comunicate with the RS api you'll need to configure and validate a token to do this work.

1. Create an auth token:
    ```
    echo -n 'nginx:password' | base64
    ```

2. Authorize the token to the api:
    ```
    curl -u "f943c89102a9ac246a246fe4f6a6e844:prueba" -d '{ "token" : "nginx:password"}' http://127.0.0.1:9092/jsonApiServer/authorizeToken
    ```

3. Check that token was authorized:
    ```
    curl -u "f943c89102a9ac246a246fe4f6a6e844:prueba" -d '{ "token" : "nginx:password"}' http://127.0.0.1:9092/jsonApiServer/isAuthTokenValid
    ```

    After that nginx should make calls to the api, for example on your `/etc/nginx/sites-enabled/default.conf`:
    ```
    location = /rsPeers/GetRetroshareInvite {

        proxy_set_header Authorization "Basic bmdpbng6cGFzc3dvcmQ=";
        proxy_pass http://127.0.0.1:9092;
    }
    ```
    See an example of nginx configuration on [nginx-default.conf](nginx-default.conf).

    Finish the config with a `sudo service nginx restart`

4. Install all the frontend files from the [retroshare-web-bridges](https://gitlab.com/r3sistance/retroshare-web-bridges) repository:

    ```bash
    git clone https://gitlab.com/r3sistance/retroshare-web-bridges && cd retroshare-web-bridges
    sudo cp auto-join.html /var/www/html/index.html
    sudo cp footer.html /var/www/html/footer.html
    sudo cp -R css /var/www/html/css
    # From scripts you could copy just utils and auto join scripts
    sudo cp -R scripts /var/www/html/scripts
    ```

    If you want to link the documents, create a folder where to store the data, for example:

    ```bash
    mkdir /home/www-data && cd /home/www-data && git clone https://gitlab.com/r3sistance/retroshare-web-bridges && chown -R www-data: /home/www-data
    sudo -u www-data ln -s /home/www-data/retroshare-web-bridges/footer.html /home/www-data/retroshare-web-bridges/css /home/www-data/retroshare-web-bridges/scripts  /var/www/html/
    ```

Now if you enter to localhost you should see `index.hml` template with the node publickey.

## Automatizing

#### Running Retroshare as service

You can set `retroshare-service` at startup using systemd script on [scripts/retroshare.service](scripts/retroshare.service) adding it to `/etc/systemd/system/retroshare.service` and then:

```
systemctl enable retroshare.service
systemctl start retroshare.service
```

If you do that is not necessary to run retroshare-service from crontab as described below.

#### Configuring `login.sh`

This script will login into Retroshare if is not logged in. Additionally creates a `.json` file with information about the node. Enter inside and configure the parameters. Make it sure that `LOCATIONINFOSTORE` is pointing to a write permissions file.

Once configured, execute it with `./login.sh` to check everything is working.

Now you can make login.sh working as service in the same way as before: copying the [scripts/retroshare-login.service](scripts/retrohsare-login.service) and adding it to `/etc/systemd/system/retroshare-login.service`.

As this service is `Type=oneshot` we will run it using cron each 10 minutes. But we let the **TODO** of just run it using systemd. So making `crontab -e`:

```bash
# each X minutes try to login to RS
*/10 * * * * (($HOME/login.sh &> $HOME/rsLogin.log)&)
```

#### Setting up `buryTheDead.py`

The script [`buryTheDead.py`](scripts/buryTheDead.py) is used to delete nodes that are offline from 90 days ago.

To setting it up just authorize a token on your RS relay as seen on the step "Setting up automatic key acceptation", creating an authorization token for the script. Open the script and configure the necessary parameters. After all just create a crontab to execute it:

```bash
#Ansible: Tier1 bury the dead
@weekly (($HOME/buryTheDead.py &> $HOME/buryTheDead.log)&)
```

## Best configurations

#### Forwarding ports to direct connection

On a Retroshare certificate are bundled the IP's of the node. We can take a look making something like `cat cert | base64 -d`. If you check this you realize that the certificate has information about the node ip:port. The first connection type of other node to the relay will be direct connection. So, if our relay is under a NAT, is a best practice to open a port that point directly to our relay to make easier for other nodes to find it and connect it. Lets see how to do it:

To make easier to connect the other nodes to the relay we choose the port 443 because is normally opened to any type of network. So we will also move the `auto-join` https web interface to the 8080 port.

1. First create the appropriate ip tables rules. Look at file [howto-iptables.md](howto-iptables.md) to open and forward the apropiate ports.

2. Configure the relay to put his external port on 443. Where:
  * `sslId` : is `mLocationId` that you can find under `/rsLoginHelper/getLocations`
  * `addr` : is the external ip of your RS machine.
  * `port` : the port where to listen

    ```
    curl -u "f943c89102a9ac246a246fe4f6a6e844:prueba" -d '{ "sslId" : "f943c89102a9ac246a246fe4f6a6e844" , "addr" : "<IP>" , "port" : 443}' http://127.0.0.1:9092/rsPeers/setExtAddress
    ```

    Note that the port is write without quotes!


3. Configure nginx to serve https on port 8080 and redirect all the traffic. Look at [nginx-default.conf](nginx-default.conf) to see an example.

## Some other notes (TL;DR)

### Moving and backuping locations

Copying all `.retroshare` directory will backup all locations.

##### Single location
If you want to move or backup just one location follow next steps (when logged in):
1. Get location pgpId and locationId to use it later.
    ```
    curl http://127.0.0.1:9092/rsLoginHelper/getLocations
    LOCATIONID="f943c89102a9ac246a246fe4f6a6e844"
    PGPID="C3BB136ABC43E2E0"
    ```

2. Export the keys to a file. For some reason `/rsAccounts/ExportIdentity` that should save directly the keypair into a file doesn't work for write permissions. You can find LOCATIONID parameter and pgpId the step before.
    ```
    curl -u "$LOCATIONID:prueba" -d '{ "pgpId":"'$PGPID'"}' http://127.0.0.1:9092/rsAccounts/ExportIdentityToString | jq -j '.data' > ~/.retroshare/LOC06_$LOCATIONID/keys.pgp
    ```

3. Scp the content to the new server:
    ```
    scp -rp  ~/.retroshare/LOC06_$LOCATIONID ssh@server.net:.retroshare
    ```

4. Delete backup file
    ```
    rm ~/.retroshare/LOC06_$LOCATIONID/keys.pgp
    ```

5. On the new server import the pgp keys (make sure Retroshare service is running and you are on `~/.retroshare/LOC06_$LOCATIONID/`):
    ```
    export  KEYSPATH=$(readlink -f keys.pgp)
    curl -d '{ "filePath": "'"${KEYSPATH}"'"}' http://127.0.0.1:9092/rsAccounts/importIdentity
    ```

    Then restart Retroshare.

6. Delete key  file
    ```
    rm $KEYSPATH
    ```

7. After that check login as usual.


#### Configuring cron

**NOT NEEDED: this following step is just an example of configuration if you want to add more scripts or use the crontab instead the .service**

To set up RetroShare at startup you can copy this cronrules extracted from `scripts/dctriers.yml`:

    ```
    #Ansible: Tier1 start retroshare-service at boot
    @reboot ((retroshare-service &> $HOME/retroshare.log)&)
    #Ansible: Tier1 restart retroshare-service if crashed
    */5 * * * * pgrep retroshare &> /dev/null || ((retroshare-service &> retroshare.log)&)
    #Login: login to retroshare and create info.json file
    */6 * * * * ((login.sh &> rsLogin.log)&)
    #Ansible: Tier1 bury the dead
    @weekly (($HOME/Tier1/buryTheDead.py &> $HOME/buryTheDead.log)&)
    #Ansible: Tier1 auto-subscribe channels
    * * * * * ((qml -apptype core $HOME/Tier1/channels_update.qml &> $HOME/channels.log)&)
    #Ansible: Tier1 file cache housekeeping
    */10 * * * * (($HOME/Tier1/file_cache_housekeeping.sh &> $HOME/cache_housekeeping.log)&)
    ```

Notes:
* **login to retroshare and create info.json file**:  Bash script to login to retroshare-service. Also can create a `information.json` file to store data of the node. Used by `index.html` to show node info.
* **Tier1 bury the dead**: this script delete friend nodes that are inactive for X time. For defect 90 days.
* **Tier1 auto-subscribe channels** (OPTIONAL): this script will auto-subscribe to every channel that the node see. Have QT dependencies, look at [original repo](https://gitlab.com/RepositorioDeCultura/Tier1) for more info. Set autodownload of all files on all channels.
* **Tier1 file cache housekeeping** (OPTIONAL): delete oldest Retroshare downloaded files when there are no disc space. Used for example when `channels_update.qml` is used and the autodownload is activated.


### RetroShare API

Retroshare only listen on 127.0.0.1 so the api exposed will be only on nignx config file (and will be under reverse proxy).

### Dynamic Ip and port forwarding.

If you need to forward tcp/udp ports because you are behind NAT/Proxy, check:
* Forward TCP/UDP external port:  view `scripts/dctriers.yml` on _Forward external TCP port_ & _Forward external UDP port_.
* Configure TCP/UDP internal port:  view `scripts/dctriers.yml` on _Configure RetroShare Internal Port_.

If you have dynamic public Ip you can configure a DynDns to help other nodes to find you. See: `scripts/dctriers.yml` line 253.
