fast report on iptables

check howto do a [persistent iptables](https://gitlab.com/guifi-exo/wiki/blob/master/howto/debianVM.md#persistent-firewall)

```
*mangle
# content about mangle (?) mss tcp?
COMMIT

*filter
# input, output or forward
-A INPUT -p icmp -j ACCEPT
-A INPUT ! -i eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT -m comment --comment "general policy except for eth0"
-A INPUT -i lo -j ACCEPT
-A INPUT -i eth0 -j ACCEPT
-A INPUT -i eth1 -p tcp -m multiport --dports 80,8080 -j ACCEPT -m comment --comment "HTTP/HTTPS guifi"
-A INPUT -i eth2 -p tcp -m multiport --dports 80,8080 -j ACCEPT -m comment --comment "HTTP/HTTPS Internet"

-A INPUT -i eth2 -p tcp -m multiport --dports 9789 -j ACCEPT -m comment --comment "ssh Internet"

-A INPUT -i eth1 -p tcp -m multiport --dports 443 -j ACCEPT -m comment --comment "retroshare external port guifi"
-A INPUT -i eth2 -p tcp -m multiport --dports 443 -j ACCEPT -m comment --comment "retroshare external port Internet"

-A INPUT -i eth1 -p tcp -m multiport --dports 5702 -j ACCEPT -m comment --comment "retroshare internal port guifi"
-A INPUT -i eth2 -p tcp -m multiport --dports 5702 -j ACCEPT -m comment --comment "retroshare internal port Internet"

-A INPUT -j DROP
COMMIT

*nat
-A PREROUTING -p tcp -m tcp --dport 443 -j DNAT --to-destination 192.168.95.35:5702 -m comment --comment "retroshare external port 443 to retroshare internal port 5702"
COMMIT
```

we are forwarding 443 port to an internal retroshare (in this case 5702) port but we cannot use `j DNAT` we have to use `-j REDIRECT` src https://unix.stackexchange.com/a/249183
