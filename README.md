Important references:

- In development version there is a new API JSON to develop it as in C++ but without touching C++ -> https://github.com/RetroShare/RetroShare/tree/master/jsonapi-generator
- Retroshare deployment using docker and ansible: https://gitlab.com/RepositorioDeCultura/Tier1
- Working example http://pool.elrepo.io/
- https://gitlab.com/r3sistance/retroshare-web-bridges
