# Channels cheatset

### Channels
* Get all channels
curl -u "$ACCOUNT:$PASSWORD"  http://127.0.0.1:9092/rsGxsChannels/getChannelsSummaries

* Get single channel
CHANNELID=<chanid>
curl -u "$ACCOUNT:$PASSWORD" -d '{ "chanIds": ["'$CHANNELID'"] }'  http://127.0.0.1:9092/rsGxsChannels/getChannelsInfo

* Create Channel
curl -u "$ACCOUNT:$PASSWORD" -d '{ "channel":{ "mMeta":{ "mGroupName":"JSON test group", "mGroupFlags":4, "mSignFlags":520 }, "mDescription":"JSON test group description" }, "caller_data":"" }' http://127.0.0.1:9092/rsGxsChannels/createChannel

* Subscribe/unsusbscribe
curl -u "$ACCOUNT:$PASSWORD" -d '{ "channelId" : "''$CHANNELID''" , "subscribe" : "false"}'  http://127.0.0.1:9092/rsGxsChannels/subscribeToChannel

### Channels posts

* Get post summaries
curl -u "$ACCOUNT:$PASSWORD" -d '{ "channelId": "''$CHANNELID''" }'  http://127.0.0.1:9092/rsGxsChannels/getContentSummaries

* Get specific post data
CONTENTID=<contentId>
curl -u "$ACCOUNT:$PASSWORD" -d '{ "channelId": "''$CHANNELID''", "contentsIds": ["'$CONTENTID'"] }'  http://127.0.0.1:9092/rsGxsChannels/getChannelContent

* Create post
curl -u "$ACCOUNT:$PASSWORD" -d '{"post" : {"mMeta": {"mGroupId" : "'$CHANNELID'","mMsgFlags" : 0, "mMsgName" : "title"},"mMsg" : "mesaaagee"}, "caller_data":""}'  http://127.0.0.1:9092/rsGxsChannels/createPost

* Create post with file
**REMEMBER TO ADD THE DIR TO SHARED DIRS (see guide file sharing)**
```bash
RSFILEPATH="/path/containing/string";
MHASH=$(sha1sum $RSFILEPATH|cut -f1 -d' ')
MSIZE=$(du -b $RSFILEPATH|cut -f1 )  NOT WORK THIS RETURN STRING NOT INT!!!

curl -u "$ACCOUNT:$PASSWORD" -d '{"post" : {"mMeta": {"mGroupId" : "1ae5c7379642b89b6f628df3ad4dd0b5","mMsgFlags" : 0, "mMsgName" : "title"},"mMsg" : "mesaaagee","mFiles" : [{mName : "'$RSFILEPATH'",mHash : "'$MHASH'",mSize : '$MSIZE'}]}, "caller_data":""}'  http://127.0.0.1:9092/rsGxsChannels/createPost
```
