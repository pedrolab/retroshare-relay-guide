* [retroshare.service](retroshare.service) : A systemd service to let retroshare working. Put it on `/etc/systemd/system/retroshare.service` and then just `systemctl enable retroshare.service && systemctl start retroshare.service`.
* [retroshare-login.service](retroshare-login.service) : A systemd service to let retroshare working. Put it on `/etc/systemd/system/retroshare-login.service` and control that `ExecStart` point to login.sh.
* [login.sh](login.sh) : Bash script to login to retroshare-service. Also can create a `information.json` file to store data of the node. Used by index.html.
* [dctriers.yml](dctriers.yml) : Is an Ansible script to install [tiers](https://gitlab.com/RepositorioDeCultura/Tier1) repo and the retroshare service.
* [buryTheDead.py](buryTheDead.py) : this script delete friend nodes that are inactive for X time. For defect 90 days.
* [channels_update.qml](channels_update.qml) : this script will auto-subscribe to every channel that the node see. Have QT dependencies, look at original repo for more info. Set autodownload of all files on all channels.
* [file_cache_housekeeping.sh](file_cache_housekeeping.sh) : delete oldest Retroshare downloaded files when there are no disc space. Used for example when channels_update.qml is used and the autodownload is activated.
