#!/bin/bash

MIN_FREE_GIGA="10"

get_free_giga()
{
	freeBytes=$(df /home/retroshare/.retroshare/ | awk '/[0-9]%/{print $(NF-2)}')
	echo $((freeBytes/1048576))
}

[ "$(get_free_giga)" -le "$MIN_FREE_GIGA" ] &&
{
	ls -tr /home/retroshare/.retroshare/LOC06_*/Downloads/** | while read dFile; do
		[ -e "${dFile}" ] &&
		{
			echo "Freeing $(stat --printf="%s" "${dFile}")bytes by deleting ${dFile}"
			rm -rf "${dFile}"
			[ "$(get_free_giga)" -gt "$MIN_FREE_GIGA" ] && break
		}
	done
}

[ "$(get_free_giga)" -le "$MIN_FREE_GIGA" ] &&
{
	ls -tr /home/retroshare/.retroshare/LOC06_*/Partials/** | while read dFile; do
		[ -e "${dFile}" ] &&
		{
			echo "Freeing $(stat --printf="%s" "${dFile}")bytes by deleting ${dFile}"
			rm -rf "${dFile}"
			[ "$(get_free_giga)" -gt "$MIN_FREE_GIGA" ] && break
		}
	done
}

