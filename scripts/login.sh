#!/bin/bash

RSJSONAPIURL="http://127.0.0.1:9092/"
RSPROFILEPASSWORD="0000"
LOCATION="" # If empty use /rsLoginHelper/getLocations[0]
LOCATIONINFOSTORE="/var/www/html/info.json" # Where to store location info


# Get curl command
# $1 api path
# $2 options
function getCurl {
  #[ -z "$2" ] && optData="" || optData="-d "
  opts=" -s --fail "$2 # Silent curl output, handling fail
  echo "curl $opts $RSJSONAPIURL$1"
}

# Execute curl command
function rsJsonApiRequest {
  res=$($1)
  if [ -z "$res" ];then
    echo "ERROR: Curl failed, check connection with socket"
    exit 1
  else
    echo "$res"
  fi
}

# Get location info and store it on $LOCATIONINFOSTORE
function storeLocationInfo {
  checkForLocation
  c=$(getCurl "/rsLoginHelper/getLocations")
  echo "ApiCall getLocations: $c"
  info=$(rsJsonApiRequest "$c" | jq '.locations[] | select(.mLocationId=="'$LOCATION'")')
  if [ -z "$info" ];then
    echo "ERROR: getting location info"
  fi

  echo "$info" > $LOCATIONINFOSTORE
  if [ $? -eq 0 ]; then
    echo "Info file for $LOCATION created on $LOCATIONINFOSTORE"
  else
    echo "ERROR: writing $LOCATIONINFOSTORE"
    exit 1
  fi
}

# If is no $LOCATION check for default location
function checkForLocation {
  # If no location is configured
  if [ -z $LOCATION ]; then
    c=$(getCurl "rsAccounts/getCurrentAccountId")
    echo "ApiCall getCurrentAccountId: $c"
    LOCATION=$(rsJsonApiRequest "$c"  | jq -j '.id')
    [ ! -z $LOCATION ] && echo "$LOCATION found" || $(echo "Id not found, create one" && exit 1)
  fi
}

# Make login if is not logged in
# With  defined $LOCATION or with rsAccounts/getCurrentAccountId location
function doLogin {
  c=$(getCurl "rsLoginHelper/isLoggedIn")
  echo "ApiCall isLoggedIn: $c"
  # Check if is logged in
  if [ $(rsJsonApiRequest "$c" | jq -j '.retval') = "false" ];then
    echo "Not logged in. Trying to login"

    # If no location configured get by default location
    checkForLocation

    c=$(getCurl  "rsLoginHelper/attemptLogin")
    echo "ApiCall attemptLogin $c [login options]"
    res=$($c -d '{ "account":"'$LOCATION'","password":"'$RSPROFILEPASSWORD'" }' | jq -j '.retval')
    if [ $res = "0" ];then
      echo "Successfully loged in!"
    else
      echo "ERROR: login in... Bad password?"
      exit 1
    fi
  else
    echo "Already logged in"
  fi
  return 0
}

function run {
  echo "$retroInstance is running"
  doLogin
  if [ ! -z $LOCATIONINFOSTORE ];then
    storeLocationInfo
  fi
}


# Check if retroshare-service is running
retroInstance="retroshare-service"
ps aux | grep -w $retroInstance | grep -v "grep" -q
if [ $? -eq 0 ]; then
  run
else
  echo "ERROR: $retroInstance is not runing" && exit 1;
fi
