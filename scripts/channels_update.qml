/*
 * elRepo.io
 * Copyright (C) 2018  Gioacchino Mazzurco <gio@eigenlab.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQml 2.2

QtObject
{
	id: rootElement

	property string rsJsonApiUrl: "http://127.0.0.1:9092/"
	property string rsProfilePassword: "0000"
	property string rsJsonApiAuthToken
	property int pendingJsonApiRequests: 0
	property int softInterval_ms: 130
	property int hardTimeout_ms: 30000

	Component.onCompleted: main()

	/*bool*/ function isSubscribed(/*RsGroupMetaData*/ channelMeta)
	{
		/// @see GXS_SERV::GROUP_SUBSCRIBE_SUBSCRIBED = 0x04 in C++ code
		if(channelMeta.mSubscribeFlags & 4) return true;
		return false;
	}

	function main()
	{
		rsJsonApiRequest( "/rsLoginHelper/isLoggedIn", "",
			function(p)
			{
				var jsonAns = JSON.parse(p);
				if(jsonAns.retval)
				{
					rsJsonApiRequest( "/rsAccounts/getCurrentAccountId", "",
						function(p)
						{
							var jsonAns = JSON.parse(p);
							rootElement.rsJsonApiAuthToken =
								jsonAns.id + ":" + rootElement.rsProfilePassword
							processChannels()
						}
					)
				}
				else
				{
					rsJsonApiRequest("/rsLoginHelper/getLocations","",
						function(p)
						{
							var jsonAns = JSON.parse(p);
							var jsonData =
								{
									account: jsonAns.locations[0].mLocationId,
									password: rootElement.rsProfilePassword
								}
							rsJsonApiRequest(
								"/rsLoginHelper/attemptLogin",
								JSON.stringify(jsonData),
								function(p)
								{
									var jsonAns = JSON.parse(p);
									if(jsonAns.retval === 0)
									{
										rootElement.rsJsonApiAuthToken =
											jsonAns.locations[0] + ":" + rootElement.rsProfilePassword
										processChannels()
									}
									else
									{
										console.log("Login failed!", jsonAns.retval)
										Qt.exit(-1)
									}
								}
							)
						}
					)
				}
			}
		)

		var waitTime = 0
		while(waitTime < hardTimeout_ms)
		{
			waitTime += softInterval_ms
			delayed(waitTime, function()
			{
				if(pendingJsonApiRequests === 0)
				{
					console.log("Safely quitting without pending requests.")
					Qt.quit()
				}
			})
		}

		delayed(hardTimeout_ms, function()
		{
			console.log(
				"Force quitting after hardTimeout_ms", hardTimeout_ms,
				"ms has been hitted! With", pendingJsonApiRequests,
				"requests still pending. This should not happen!" )
			Qt.exit(-2)
		})
	}

	function processChannels()
	{
		rsJsonApiRequest( "/rsGxsChannels/getChannelsSummaries", "",
			function(p)
			{
				var channelsSummariesArr = JSON.parse(p).channels

				for (var i = 0; i < channelsSummariesArr.length; ++i)
				{
					if(!isSubscribed(channelsSummariesArr[i]))
					{
						console.log(
							"Subscribing to:",
							channelsSummariesArr[i].mGroupId,
							channelsSummariesArr[i].mGroupName )

						subscribeChannel(channelsSummariesArr[i].mGroupId)
					}
				}

				var channelsIdArr = []
				for (var i = 0; i < channelsSummariesArr.length; ++i)
				{
					channelsIdArr.push(channelsSummariesArr[i].mGroupId)
				}

				if(channelsIdArr.length < 1)
				{
					console.log("channelsIdArr empty nothing more to do")
					return;
				}

				rsJsonApiRequest( "/rsGxsChannels/getChannelsInfo",
					JSON.stringify({chanIds: channelsIdArr}),
					function(p)
					{
						var channelsInfoArr = JSON.parse(p).channelsInfo
						for (var i = 0; i < channelsInfoArr.length; ++i)
						{
							if( isSubscribed(channelsInfoArr[i].mMeta)
								&& !channelsInfoArr[i].mAutoDownload )
							{
								console.log(
									"Setting autodownload for channel:",
									channelsInfoArr[i].mMeta.mGroupId,
									channelsInfoArr[i].mMeta.mGroupName )

								autoDownloadChannel(channelsInfoArr[i].mMeta.mGroupId)
							}
						}
					}
				)
			}
		)
	}

	function subscribeChannel(channel_id)
	{
		console.log("subscribeChannel(channel_id)", channel_id)
		var data = { channelId: channel_id, subscribe: true }

		rsJsonApiRequest(
			"/rsGxsChannels/subscribeToChannel",
			JSON.stringify(data) )
	}

	function autoDownloadChannel(channel_id)
	{
		console.log("autoDownloadChannel(channel_id)", channel_id)

		var data = { channelId: channel_id, enable: true }
		rsJsonApiRequest( "/rsGxsChannels/setChannelAutoDownload",
						  JSON.stringify(data) )
	}

	function rsJsonApiRequest(path, data, callback)
	{
		++pendingJsonApiRequests
		console.log("rsJsonApiRequest(path, data, callback)", path, data, callback)
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function()
		{
			if(xhr.readyState === 4)
			{
				console.log( path, "callback", xhr.status,
							 xhr.responseText.replace(/\s+/g, " ").
								substr(0, 60).replace(/\r?\n|\r/g," ") )
				--pendingJsonApiRequests
				if(typeof(callback) === "function") callback(xhr.responseText);
			}
		}
		xhr.open('POST', rsJsonApiUrl + path, true);
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.setRequestHeader("Authorization", "Basic "+Qt.btoa(rsJsonApiAuthToken));
		xhr.send(data);
	}

	function getTimer()
	{
		/// import QtQml 2.11 can be used with newer qt versions
		return Qt.createQmlObject("import QtQuick 2.0; Timer {}", rootElement);
	}

	function delayed(millisec, callback) {
		var timer = new getTimer();
		timer.interval = millisec;
		timer.repeat = false;
		timer.triggered.connect(callback);
		timer.start();
	}
}
