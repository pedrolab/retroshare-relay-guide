#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
elRepo.io
Copyright (C) 2018  Gioacchino Mazzurco <gio@eigenlab.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Thanks to sehraf for releasing the original version as public domain
https://github.com/sehraf/python-retroshare/blob/master/buryTheDead.py
https://github.com/sehraf/python-retroshare/blob/master/LICENSE
"""

import json, requests, time, math

jsonApiUrl="http://127.0.0.1:9092/"

debug = False

user = 'buryTheDead'
pw = 'inNodeCementery'
groupName = "Graveyard"
offlineLimit = 90 # days


def debugDump(label, data):
	if not debug: return
	print(label, json.dumps(data, sort_keys=True, indent=4))

def jsonApiCall(function, data = None):
	url = jsonApiUrl + function

	debugDump('POST: ' + url, data)
	resp = requests.post(url=url, json=data, auth=(user, pw))

	debugDump('RESP', resp.json())
	return resp.json()

class rsGroup:
	def __init__(self, name):
		self.name = name
		self.info = {}

		# init info
		req = {'groupName': name}
		resp = jsonApiCall('/rsPeers/getGroupInfoByName', req)

		if resp['retval']:
			self.info = resp['groupInfo']
		else:
			self.info = {
				'name': self.name,
				'id': 0,
				'flag': 0,
				'peerIds': []
			}

			req = {'groupInfo': self.info}
			# assume no error here
			jsonApiCall('/rsPeers/addGroup', req)

			req = {'groupName': self.name}
			self.info = jsonApiCall('/rsPeers/getGroupInfoByName', req)['groupInfo']

	def delete(self):
		req = {'groupInfo': self.info}
		jsonApiCall('/rsPeers/removeGroup', req)
		self.info = {}

	def addPeer(self, peer):
		self.assignPeer(peer, True)

	def removePeer(self, peer):
		self.assignPeer(peer, False)

	def addPeers(self, peers):
		self.assignPeers(peers, True)

	def removePeers(self, peers):
		self.assignPeers(peers, False)

	def assignPeer(self, peer, assign):
		req = {
				'groupId': self.info['id'],
				'peerId': peer,
				'assign': assign
			}
		resp = jsonApiCall('/rsPeers/assignPeerToGroup', req)
		if resp['retval']:
			if assign:
				self.info['peerIds'].append(peer)
			else:
				self.info['peerIds'].remove(peer)

	def assignPeers(self, peers, assign):
		req = {
				'groupId': self.info['id'],
				'peerIds': peers,
				'assign': assign
			}
		resp = jsonApiCall('/rsPeers/assignPeersToGroup', req)

	def isAssigned(self, pgpId):
		return pgpId in self.info['peerIds']

	def update(self):
		req = {'groupName': self.name}
		self.info = jsonApiCall('/rsPeers/getGroupInfoByName', req)['groupInfo']

def getDays(details):
	days = time.time() - details['lastConnect']
	days = days / 3600 / 24
	days = math.ceil(days)
	return days

if __name__ == "__main__":
	group   = rsGroup(groupName)
	friends = jsonApiCall('/rsPeers/getFriendList')['sslIds']

	# we go though the list of locations
	# -> we can have one friend with long time offline locations and recent locations
	# -> first move to group then remove
	toAdd = []
	toRemove = []
	names = {}
	days = {}

	# add long offline friends
	for friend in friends:
		req = {'sslId': friend}
		details = jsonApiCall('/rsPeers/getPeerDetails', req)['det']
		pgpId = details['gpg_id']

		d = getDays(details)

		if d > offlineLimit:
			if not pgpId in toAdd:
				toAdd.append(pgpId)
				names[pgpId] = details["name"]
				days[pgpId] = d

	# remove recent online friends
	for friend in friends:
		req = {'sslId': friend}
		details = jsonApiCall('/rsPeers/getPeerDetails', req)['det']
		pgpId = details['gpg_id']

		d = getDays(details)

		if d < offlineLimit:
			if pgpId in toAdd:
				toAdd.remove(pgpId)
			if group.isAssigned(pgpId):
				toRemove.append(pgpId)
				names[pgpId] = details["name"]
				days[pgpId] = d

	# ensure we have an updated peer list
	group.update()

	# toAdd now contains only the peers that are too long offline (excluding those with one recent online and one too long offline location)
	for pgpId in toAdd:
		if not group.isAssigned(pgpId):
			print("burrying " + names[pgpId] + "\t(offline for " + str(days[pgpId]) + " days)")
		else:
			toAdd.remove(pgpId)
	group.addPeers(toAdd)

	# ensure we have an updated peer list
	group.update()

	for pgpId in toRemove:
		if group.isAssigned(pgpId):
			print("unearthing " + names[pgpId] + "\t(offline for " + str(days[pgpId]) + " days)")
		else:
			toRemove.remove(pgpId)
	group.removePeers(toRemove)
